$(document).ready(function() {

$('#btn-valide').click(function(){ 
$.ajax({ 
      type: "GET",
      contentType: 'application/json; charset=utf-8',
      url: "http://localhost/got/serveur/index.php/bonjour",
     success: function(data){
         alert(data);
      }
 });
});
 
 
  $('#btn-new-liste').click(function(){ 
    let idx=$('#idx').val();
      $.ajax({ 
      type: "GET",
      url: "http://localhost/got/serveur/index.php/personnage/"+idx,
      success: function(data){  
        $("#result").html(data);
      }
    });
  });
  
     $('#new-btn').click(function(){ 
    let id=$('#id').val();
    let mdp=$('#mdp').val();
    $.ajax({ 
          type: "GET",
          contentType: 'application/json; charset=utf-8',
          url: "http://localhost/got/serveur/index.php/user?id="+id+"&mdp="+mdp,
         success: function(data){
            $("#result2").html(data);
          }
     });
    });

      $('#btn-inscription').click(function(){ 
    let login=$('#idInscription').val();
    let mdp=$('#mdpInscription').val();
    let email=$('#email').val();
    $.ajax({ 
          type: "POST",
          contentType: 'application/json; charset=utf-8',
          url: "http://localhost/got/serveur/index.php/user?email="+email+"&idInscription="+login+"&mdpInscription="+mdp,
         success: function(data){
             $("#result3").html("L'utilisateur a été enregistré");
          }
     });
    });

      $('#btn-delete').click(function(){
    let loginDel=$('#loginDelete').val();
    $.ajax({
      type: "DELETE",
      contentType: 'application/json; charset=utf-8',
      url: "http://localhost/got/serveur/index.php/user/"+loginDel,
      success: function(data){
        $("#result4").html("L'utilisateur a été supprimé");
      }
    })
  });
      $('#btn-change').click(function(){
        let changeLogin = $('#identifLogin').val();
        let newEmail = $('#changeEmail').val();
        $.ajax({
          type: "PUT",
          contentType: 'application/json; charset=utf-8',
          url: "http://localhost/got/serveur/index.php/user?identifLogin="+changeLogin+"&changeEmail="+newEmail,
          success: function(data){
            $("#result5").html("L'email a été modifié");
          }
        })
      });

      $('#btn-100').click(function(){
        $('#tabPerso').bootstrapTable({
          url: "http://localhost/got/serveur/index.php/personnage",
          columns: [{
            field: 'name',
            title: 'Nom du personnage'
          }, {
            field: 'titles',
            title: 'Titre du personnage'
          }, {
            field:'culture',
            title:'Culture du personnage'
          },]
        });
      });

      $('#btn-user-list').click(function(){
        $('#tabUsers').bootstrapTable({
          url:"http://localhost/got/serveur/index.php/users",
          columns: [{
            field: "id",
            title: "Identifiant"
          }, {
            field: "login",
            title: "Login de l'utilisateur"
          }, {
            field: "mdp",
            title: "Mot de passe de l'utilisateur"
          },{
            field: "email",
            title: "Email de l'utilisateur"
          },] 
          });
        });
});