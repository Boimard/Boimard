$(document).ready(function() {

$('#btn-valide').click(function(){ 
$.ajax({ 
      type: "GET",
      contentType: 'application/json; charset=utf-8',
      url: "http://localhost/got/serveur/index.php/bonjour?token="+ sessionStorage.getItem("token"),
     success: function(data){
         alert(data);
      },
    error: function(){
      alert(" Vous n'etes pas connecté");
    }
 });
});
 
 
  $('#btn-new-liste').click(function(){ 
    let idx=$('#idx').val();
      $.ajax({ 
      type: "GET",
      url: "http://localhost/got/serveur/index.php/personnag?idx="+idx+"&token="+sessionStorage.getItem("token"),
      success: function(data){  
        $("#result").html(data);
      }          
      });
    });
  
     $('#new-btn1').click(function(){ 
    let id=$('#id').val();
    let mdp=$('#mdp').val();
    $.ajax({ 
          type: "GET",
          contentType: 'application/json; charset=utf-8',
          url: "http://localhost/got/serveur/index.php/user?token="+ sessionStorage.getItem("token")+"&id="+id+"&mdp="+mdp,
         success: function(data){
            $("#result2").html(data);
          },
          error: function(){
      alert(" Vous n'etes pas connecté");
        }
     });
    });

      $('#btn-inscription').click(function(){ 
    let login=$('#idInscription').val();
    let mdp=$('#mdpInscription').val();
    let email=$('#email').val();
    $.ajax({ 
          type: "POST",
          contentType: 'application/json; charset=utf-8',
          url: "http://localhost/got/serveur/index.php/user?email="+email+"&idInscription="+login+"&mdpInscription="+mdp,
         success: function(data){
             $("#result3").html("L'utilisateur a été enregistré");
          }
     });
    });

      $('#btn-delete').click(function(){
    let loginDel=$('#loginDelete').val();
    $.ajax({
      type: "DELETE",
      contentType: 'application/json; charset=utf-8',
      url: "http://localhost/got/serveur/index.php/user?token="+sessionStorage.getItem("token")+"&loginDel="+loginDel,
      success: function(data){
        $("#result4").html("L'utilisateur a été supprimé");
      }
    })
  });
      $('#btn-change').click(function(){
        let changeLogin = $('#identifLogin').val();
        let newEmail = $('#changeEmail').val();
        $.ajax({
          type: "PUT",
          contentType: 'application/json; charset=utf-8',
          url: "http://localhost/got/serveur/index.php/user?token="+sessionStorage.getItem("token")+"&identifLogin="+changeLogin+"&changeEmail="+newEmail,
          success: function(data){
            $("#result5").html("L'email a été modifié");
          },
          error: function(){
            alert(" Vous n'etes pas connecté");
          }
        })
      });

      $('#btn-100').click(function(){
        $('#tabPerso').bootstrapTable({
          url: "http://localhost/got/serveur/index.php/personnage",
          columns: [{
            field: 'name',
            title: 'Nom du personnage'
          }, {
            field: 'titles',
            title: 'Titre du personnage'
          }, {
            field:'culture',
            title:'Culture du personnage'
          },]
        });
      });

      $('#btn-user-list').click(function(){
        $('#tabUsers').bootstrapTable({
          url:"http://localhost/got/serveur/index.php/users",
          columns: [{
            field: "id",
            title: "Identifiant"
          }, {
            field: "login",
            title: "Login de l'utilisateur"
          }, {
            field: "mdp",
            title: "Mot de passe de l'utilisateur"
          },{
            field: "email",
            title: "Email de l'utilisateur"
          },] 
          });
        });

      $("#btn-token").click(function(event) {               
        $.ajax({ 
          type: "GET",
          url: "http://localhost/got/serveur/index.php/obtentionToken",
          data: "login=" + $("#id").val() + "&pass=" + $("#mdp").val(),
            success: function(data){
              sessionStorage.setItem('token', data);
              document.location.href="http://localhost/got/client/firstSlim.html"                               
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {      
              $(".form-group-password").addClass("has-danger");
              $("#mdp").addClass("form-control-danger");
            }             
          });
    });


      $("#btn-house-graph").click(function(){
        var nombrePerso = new Array();
        var nomMaison = new Array();
        $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "http://localhost/got/serveur/index.php/stathouse",
        success: function(data){
            nbPersoParMaison = JSON.parse(data);

           for (var i = 0; i < nbPersoParMaison.length; i++) {
        nombrePerso[i] = nbPersoParMaison[i].nb;
        nomMaison[i] = nbPersoParMaison[i].name;

         var backgroundColor=[
        'rgba(0,101,80,0.7)',
        'rgba(0,30,80,0.7)',
        'rgba(127,9,80,0.7)',
        'rgba(255,57,10,0.7)',
        'rgba(255,175,0,0.7)',
        'rgba(0,175,255,0.7)',
      ];

      var borderColor='rgba(0,0,0,0.5)';

      var ctx = document.getElementById("graph1").getContext('2d');
      var graph1 = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: nomMaison,
          datasets:[{
            label:'nombre de personnage appartenant à la maison',
            data: nombrePerso,
            backgroundColor: backgroundColor,
            borderColor: borderColor,
            borderWidth:1,
          }],
        }
      });
      }
    }
  });
});
      

      $("#btn-cities-graph").click(function(){
        var nombreVille = new Array;
        var villeType = new Array;
        $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "http://localhost/got/serveur/index.php/statcities",
        success: function(data){
            nbVilleParType = JSON.parse(data);
            for (var i = 0; i < nbVilleParType.length; i++) {
        nombreVille[i] = nbVilleParType[i].nb;
        villeType[i] = nbVilleParType[i].type;
        }

        var backgroundColor=[
        'rgba(0,101,80,0.7)',
        'rgba(0,30,80,0.7)',
        'rgba(127,9,80,0.7)',
        'rgba(255,57,10,0.7)',
        'rgba(255,175,0,0.7)',
        'rgba(0,175,255,0.7)',
      ];

      var borderColor='rgba(0,0,0,0.5)';

      var ctx2 = document.getElementById("graph2").getContext('2d');
      var graph1 = new Chart(ctx2, {
        type: 'doughnut',
        data: {
          labels: villeType,
          datasets:[{
            label:'Nombre de ville appartenant à ce type',
            data: nombreVille,
            backgroundColor: backgroundColor,
            borderColor: borderColor,
            borderWidth:1,
          }],
             labels: [
        'Other',
        'Castle',
        'City',
        'Town',
        '',
        'Ruin'
    ],    
      }
    });
        }
        }); 
  });


      $("#btn-perso-graph").click(function(){
        var nombrePerso50 = new Array;
        $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "http://localhost/got/serveur/index.php/statperso",
        success: function(data){
            nbPersoPar50 = JSON.parse(data);
        }
        });

        for (var i = 0; i < nbPersoPar50.length; i++) {
        nombrePerso50[i] = nbPersoPar50[i].nb;
        }

        var backgroundColor=[
        'rgba(0,101,80,0.7)',
        'rgba(0,30,80,0.7)',
        'rgba(127,9,80,0.7)',
        'rgba(255,57,10,0.7)',
        'rgba(255,175,0,0.7)',
        'rgba(0,175,255,0.7)',
        'rgba(255,0,0,0.5)',
        'rgba(0,255,0,0.5)',
      ];

      var borderColor='rgba(0,0,0,0.5)';
      var ctx3 = document.getElementById("graph3").getContext('2d');
      var graph3 = new Chart(ctx3, {
        type:'bar',
        data: {
          labels: ["-100 à -50", "-50 à 0", "0 à 50", "50 à 100", "100 à 150", "150 à 200", "200 à 250", "250 à 300"],
          datasets:[{
            label:'nombre de personnage né lors de cette période',
            data: nombrePerso50,
            backgroundColor: backgroundColor,
            borderColor: borderColor,
            borderWidth:1,
          }],
         options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
      }
    }
      })


      })
});
