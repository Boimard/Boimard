SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `COLLEC`;
CREATE TABLE `COLLEC` (
  `numC` int(11) NOT NULL,
  `dateL` date DEFAULT NULL,
  `nomC` varchar(20) DEFAULT NULL,
  `harmo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`numC`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `PRODUIT`;
CREATE TABLE `PRODUIT` (
  `numP` char(3) NOT NULL,
  `desi` varchar(20) DEFAULT NULL,
  `coul` varchar(20) DEFAULT NULL,
  `prixHT` float DEFAULT NULL,
  `numC` int(11) DEFAULT NULL,
  PRIMARY KEY (`numP`),
  KEY `FK_PRODUIT` (`numC`),
  CONSTRAINT `FK_PRODUIT` FOREIGN KEY (`numC`) REFERENCES `COLLEC` (`numC`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `PRODUIT` (`numP`, `desi`, `coul`, `prixHT`, `numC`) VALUES
('A12',	'Chaise longue',	'Marine',	90,	1),
('A14',	'Serviette de bain',	'Orangé',	65,	2),
('A15',	'Coussin',	'Paille',	12,	2);