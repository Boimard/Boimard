import java.util.Scanner;
public class Billeterie {
	public static void main(String[] args){
		Scanner Sc = new Scanner (System.in);
		System.out.println("Veuillez saisir votre age.");
		int age = Sc.nextInt();
		while (age < 0){
			System.out.println("Erreur ! L'age est négatif, veuillez l'insérer de nouveau.");
		 age = Sc.nextInt();
		}
		if (age >= 0){
			if (age  < 3){
				System.out.println("C'est un nourrisson");
			}
			else if (age >= 3 && age < 12)
				System.out.println("C'est un enfant");
			else if (age >= 12 && age < 18)
				System.out.println("C'est un adolescent");
			else if (age >= 18 && age <= 55)
				System.out.println("C'est un adulte");
			else
				System.out.println("C'est un senior");
		}

	}
}
