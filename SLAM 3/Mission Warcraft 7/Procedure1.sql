DELIMITER //
CREATE PROCEDURE random_pet()
BEGIN
DECLARE nb_gift INT unsigned default ROUND(RAND()*100);
DECLARE random_pet int;
DECLARE random_player int;
WHILE nb_gift > 0 DO
SELECT pets_id INTO random_pet FROM pets ORDER BY RAND()LIMIT 1;
SELECT members_character_id INTO random_player FROM members ORDER BY RAND()LIMIT 1;
INSERT INTO pets_members VALUES(random_pet, random_player);
SET nb_gift = nb_gift-1;
END WHILE;
END;
//