DELIMITER //
CREATE TRIGGER cancel_pal BEFORE INSERT ON members
FOR EACH ROW
BEGIN
IF NEW.members_character_class = 2 AND (NEW.members_character_race = 2 OR NEW.members_character_race = 5 OR NEW.members_character_race = 6 OR NEW.members_character_race = 8 OR NEW.members_character_race = 9 OR NEW.members_character_race = 10 OR NEW.members_character_race = 26 OR NEW.members_character_race = 27 OR NEW.members_character_race = 28) THEN
DELETE FROM inconnu;
END IF;
END;
//

DELIMITER //
CREATE TRIGGER cancel_druide BEFORE INSERT ON members
FOR EACH ROW
BEGIN
IF NEW.members_character_class = 11 AND (NEW.members_character_race = 1 OR NEW.members_character_race = 3 OR NEW.members_character_race = 4 OR NEW.members_character_race = 7 OR NEW.members_character_race = 11 OR NEW.members_character_race = 22 OR NEW.members_character_race = 25 OR NEW.members_character_race = 29 OR NEW.members_character_race = 30) THEN
DELETE FROM inconnu;
END IF;
END;
//