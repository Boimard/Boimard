DELIMITER //
CREATE TRIGGER give_mounts AFTER INSERT ON members
FOR EACH ROW
BEGIN
DECLARE random int;
SELECT mounts_creature_id INTO random FROM mounts ORDER BY RAND()LIMIT 1;
INSERT INTO mounts_members VALUES(random,NEW.members_character_id);
END;
//