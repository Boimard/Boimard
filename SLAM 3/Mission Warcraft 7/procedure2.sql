DELIMITER //
CREATE PROCEDURE random_mounts()
BEGIN
DECLARE nb_gift INT unsigned default ROUND(RAND()*100);
DECLARE nb_player INT unsigned default ROUND(RAND()*50);
DECLARE random_mount int;
DECLARE random_player int;
WHILE nb_player > 0 DO
SELECT members_character_id INTO random_player FROM members ORDER BY RAND()LIMIT 1;
WHILE nb_gift > 0 DO
SELECT mounts_creature_id INTO random_mount FROM mounts ORDER BY RAND()LIMIT 1;
SELECT members_character_id INTO random_player FROM members ORDER BY RAND()LIMIT 1;
INSERT INTO members_members VALUES(random_mount, random_player);
SET nb_gift = nb_gift-1;
END WHILE;
SET nb_player = nb_player-1;
END WHILE;
END;
//