package application;
import java.util.Date;
/**
 * @author kilia
 *
 */
public class Joueur {
	private String Nom;
	private String Prenom;
	private String Poste;
	private int Numero;
	private Date Dtenaissance;
	private String Club;
	private String Licence;
	/**
	 * Constructeur de la classe Joueur
	 * @param Nom
	 * @param Prenom
	 * @param Poste
	 * @param Numero
	 * @param Dtenaissance
	 * @param Club
	 * @param Licence
	 */
	public Joueur(String Nom, String Prenom, String Poste, int Numero, Date Dtenaissance, String Club, String Licence){
		this.Nom = Nom;
		this.Prenom = Prenom;
		this.Poste = Poste;
		this.Numero = Numero;
		this.Dtenaissance = Dtenaissance;
		this.Club = Club;
		this.Licence = Licence;
	}
	
	public Joueur() {
	
	}
	/**
	 * Une methode qui permet de r�cup�rer le nom
	 * @return Nom
	 */
	public String getNom() {
		return Nom;
	}
	/**
	 * Une methode qui permet de parametrer le nom
	 * @param
	 */
	public void setNom(String nom) {
		Nom = nom;
	}
	/**
	 * Une methode qui permet de r�cup�rer le prenom
	 * @return Prenom
	 */
	public String getPrenom() {
		return Prenom;
	}
	/**
	 * Une methode qui permet de parametrer le prenom
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		Prenom = prenom;
	}
	/**
	 * Une methode qui permet de r�cup�rer le poste
	 * @return Poste
	 */
	public String getPoste() {
		return Poste;
	}
	/**
	 * Une methode qui permet de parametrer le poste
	 * @param poste
	 */
	public void setPoste(String poste) {
		Poste = poste;
	}
	/**
	 * Une methode qui permet de r�cup�rer le numero
	 * @return Numero
	 */
	public int getNumero() {
		return Numero;
	}
	/**
	 * Une methode qui permet de parametrer le numero
	 * @param numero
	 */
	public void setNumero(int numero) {
		Numero = numero;
	}
	/**
	 * Une methode qui permet de r�cup�rer la date de naissance
	 * @return DteNaissance
	 */
	public Date getDtenaissance() {
		return Dtenaissance;
	}
	/**
	 * Une methode qui permet de parametrer la date de naissance
	 * @param dtenaissance
	 */
	public void setDtenaissance(Date dtenaissance) {
		Dtenaissance = dtenaissance;
	}
	/**
	 * Une methode qui permet de r�cup�rer le club
	 * @return Club
	 */
	public String getClub() {
		return Club;
	}
	/**
	 * Une methode qui permet de parametrer le club
	 * @param club
	 */
	public void setClub(String club) {
		Club = club;
	}
	/**
	 * Une methode qui permet de r�cup�rer la licence
	 * @return Licence
	 */
	public String getLicence() {
		return Licence;
	}
	/**
	 * Une methode qui permet de parametrer la licence
	 * @param licence
	 */
	public void setLicence(String licence) {
		Licence = licence;
	}

	@Override
	/**
	 * Methode d'affichage
	 */
	public String toString() {
		return "Joueur [Nom=" + Nom + ", Prenom=" + Prenom + ", Poste=" + Poste + ", Numero=" + Numero
				+ ", Dtenaissance=" + Dtenaissance + ", Club=" + Club + ", Licence=" + Licence + "]";
	}
	
}

