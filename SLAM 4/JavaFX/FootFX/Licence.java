package application;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.util.ArrayList;
public class Licence extends Application {
	public static void main (String[] args) {
		Application.launch(Licence.class, args);
	}
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Application FOOT");
		Group root = new Group();
		GridPane grid = new GridPane();
		StackPane stack = new StackPane();
		Scene scene = new Scene(grid, 800, 800, Color.BLACK);
		Button btn = new Button();
		
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));
		
        
        primaryStage.setScene(scene);
        
        Text scenetitle = new Text("Veuillez s�l�ctionner un joueur :");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		grid.add(scenetitle, 0, 0, 2, 1);
		
		Label userid = new Label ("Licence");
		grid.add(userid,0,1);
		
		JoueursDAO j = new JoueursDAO();
		ArrayList<Joueur> listJ;
		listJ = j.rechercheAllJ();
		
		ComboBox combo = new ComboBox();
		grid.add(combo, 1, 1);
        for (Joueur i:listJ) {
            combo.getItems().addAll(i.getLicence());
        }
        
        Button b = new Button("Ok");
        grid.add(b, 2, 1);
        
        Label nom = new Label("Nom :");
        Label pre = new Label("Prenom :");
        Label num = new Label("Num�ro :");
        Label poste = new Label("Poste :");
        Label club = new Label("Club :");
        grid.add(nom,0,2);
        grid.add(pre,0,3);
        grid.add(num,0,4);
        grid.add(poste,0,5);
        grid.add(club,0,6);
        Text repNom = new Text("");
        Text repPre = new Text("");
        Text repNum = new Text("");
        Text repPoste = new Text("");
        Text repClub = new Text("");
        grid.add(repNom,1,2);
        grid.add(repPre,1,3);
        grid.add(repNum,1,4);
        grid.add(repPoste,1,5);
        grid.add(repClub,1,6);
        
        b.setOnAction(new EventHandler<ActionEvent>() {
			 public void handle(ActionEvent e) {
	         String Licence = combo.getValue().toString();
	         Joueur select = new Joueur();
	         select = j.getJoueurById(Licence);
	         repNom.setText(select.getNom());
             repPre.setText(select.getPrenom());
             repNum.setText(String.valueOf(select.getNumero()));
             repPoste.setText(select.getPoste());
             repClub.setText(select.getClub());	
             String imageURL = "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Football_Player-_Ashleigh_Hopkins_%28Swansea_City_AFC_-_Plymouth_Argyle_FC%29_The_FA_UEFA_%27A%27_Licence_Coach_2013-04-19_20-52.jpg/220px-Football_Player-_Ashleigh_Hopkins_%28Swansea_City_AFC_-_Plymouth_Argyle_FC%29_The_FA_UEFA_%27A%27_Licence_Coach_2013-04-19_20-52.jpg";
             Image image = new Image(imageURL);  
             ImageView imageview = new ImageView(image);
             grid.add(imageview, 2, 4);
			 } 
		 });
		
        primaryStage.setScene(scene);
		primaryStage.show();
	}
	

}
