package application;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
public class Majuscule extends Application {
	public static void main (String[] args) {
		Application.launch(Majuscule.class, args);
	}
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Majuscule");
		Group root = new Group();
		GridPane grid = new GridPane();
		Scene scene = new Scene(grid, 300, 275, Color.BLACK);
		Button btn = new Button();
		
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setBackground(new Background(new BackgroundFill(Color.CRIMSON, CornerRadii.EMPTY, Insets.EMPTY)));
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));
		
		primaryStage.setScene(scene);
		
		Text scenetitle = new Text("Majuscule ou Minuscule");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		grid.add(scenetitle, 0, 0, 2, 1);
		
		Label userName = new Label("Saisir le texte : ");
		grid.add(userName, 0, 1);

		TextField userTextField = new TextField();
		grid.add(userTextField, 1, 1);
		final Text blabla = new Text();
		
		Button btn1 = new Button();
		btn1.setText("Min");
		grid.add(btn1,0,2);
		
		TextField minTextField = new TextField();
		grid.add(minTextField, 0, 3);
		
		Button btn2 = new Button();
		btn2.setText("Maj");
		grid.add(btn2,1,2);
		
		TextField majTextField = new TextField();
		grid.add(majTextField, 1, 3);
		
		 btn1.setOnAction(new EventHandler<ActionEvent>() {
			 public void handle(ActionEvent e) {
	         minTextField.setText(userTextField.getText().toLowerCase());
			 } 
		 });
		 btn2.setOnAction(new EventHandler<ActionEvent>() {
			 public void handle(ActionEvent e) {
				 majTextField.setText(userTextField.getText().toUpperCase());
			 }
		 });
		primaryStage.show();
		
	}
}

