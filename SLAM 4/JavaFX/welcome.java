package application;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
public class Welcome extends Application {
	public static void main (String[] args) {
		Application.launch(Welcome.class, args);
	}
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Welcome !");
		Group root = new Group();
		GridPane grid = new GridPane();
		Scene scene = new Scene(grid, 300, 275, Color.BLACK);
		Button btn = new Button();
		btn.setText("Valider");
		
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setBackground(new Background(new BackgroundFill(Color.CRIMSON, CornerRadii.EMPTY, Insets.EMPTY)));
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));
		
		primaryStage.setScene(scene);
		
		Text scenetitle = new Text("Welcome !");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		grid.add(scenetitle, 0, 0, 2, 1);

		Label userName = new Label("Utilisateur :");
		grid.add(userName, 0, 1);

		TextField userTextField = new TextField();
		grid.add(userTextField, 1, 1);

		Label pw = new Label("Mot de Passe :");
		grid.add(pw, 0, 2);

		PasswordField pwBox = new PasswordField();
		grid.add(pwBox, 1, 2);
		
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn.getChildren().add(btn);
		grid.add(hbBtn, 1, 4);
		
		final Text actiontarget = new Text();
        grid.add(actiontarget, 1, 6);
        
        btn.setOnAction(new EventHandler<ActionEvent>() {
        	 
            public void handle(ActionEvent e) {
                actiontarget.setFill(Color.BLACK);
                actiontarget.setText("Sign in button pressed");
            }
        });
        primaryStage.show();
	}
}
