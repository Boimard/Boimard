package eu.hautil.JoueursDAO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import eu.hautil.Joueur.Joueur;
import java.sql.ResultSet;
import java.util.ArrayList;
public class JoueursDAO {
	/**
	 * Methode pour se connecter a la base de données 
	 * @return conn
	 */
	private Connection getConnection() {
		Connection conn = null;
		String driver = "com.mysql.cj.jdbc.Driver";
		String url = "jdbc:mysql://sio-hautil.eu:3306/boimak?user=boimak&password=20000315&serverTimezone=UTC";
		String login = "boimak";
		String password = "20000315";
		try {
				Class.forName(driver);
				System.out.println("driver ok");
				conn = DriverManager.getConnection(url,login,password);
				System.out.println("Connexion ok");
		}catch(Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
	/**
	 * Methode pour ajouter un joueur a la table JoueurFoot dans la base de données
	 * @throws SQLException
	 */
	public void ajouterJ()throws SQLException{
		PreparedStatement pstmt = getConnection().prepareStatement("INSERT INTO JoueurFoot VALUES (?,?,?,?,?,?,?)");
		pstmt.setString(1,"Messi");
		pstmt.setString(2,"Lionel");
		pstmt.setString(3,"Attaquant");
		pstmt.setInt(4,22);
		pstmt.setDate(5,java.sql.Date.valueOf("1979-02-13"));
		pstmt.setString(6,"FC Barcelone");
		pstmt.setString(7,"14529278");
		int res=pstmt.executeUpdate();
		System.out.println(res);
	}
	/**
	 * Methode pour supprimer un joueur present dans la table JoueurFoot
	 * @throws SQLException
	 */
	public void supprJ() throws SQLException{
		int compteur = 0;
		PreparedStatement pstmt2 = getConnection().prepareStatement("DELETE FROM JoueurFoot WHERE Licence = 14529278");
		pstmt2.executeUpdate();
		System.out.println(pstmt2);
		compteur = compteur + 1;
		System.out.println("Lignes supprimées : " + compteur);
	}
	/**
	 * Methode pour rechercher et afficher un élément de la table JoueurFoot
	 * @return list
	 */
	public ArrayList <Joueur> rechercheAllJ(){
		ArrayList <Joueur> list = new ArrayList <Joueur>();
		try {
			PreparedStatement pstmt3 = getConnection().prepareStatement("SELECT * FROM JoueurFoot");
			ResultSet result = pstmt3.executeQuery();
			while (result.next()) {
			Joueur j = new Joueur(result.getString("Nom"),result.getString("Prenom"),result.getString("Poste"),result.getInt("Numero"),result.getDate("Date Naissance"),result.getString("Club"),result.getString("Licence"));
			list.add(j);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	private PreparedStatement prepareStatement(String reqjou) {
		return null;
	}
}
