package eu.hautil.Joueur;
import java.util.Date;
public class Joueur {
	private String Nom;
	private String Prenom;
	private String Poste;
	private int Numero;
	private Date Dtenaissance;
	private String Club;
	private String Licence;
	
	public Joueur(String Nom, String Prenom, String Poste, int Numero, Date Dtenaissance, String Club, String Licence){
		this.Nom = Nom;
		this.Prenom = Prenom;
		this.Poste = Poste;
		this.Numero = Numero;
		this.Dtenaissance = Dtenaissance;
		this.Club = Club;
		this.Licence = Licence;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public String getPrenom() {
		return Prenom;
	}

	public void setPrenom(String prenom) {
		Prenom = prenom;
	}

	public String getPoste() {
		return Poste;
	}

	public void setPoste(String poste) {
		Poste = poste;
	}

	public int getNumero() {
		return Numero;
	}

	public void setNumero(int numero) {
		Numero = numero;
	}

	public Date getDtenaissance() {
		return Dtenaissance;
	}

	public void setDtenaissance(Date dtenaissance) {
		Dtenaissance = dtenaissance;
	}

	public String getClub() {
		return Club;
	}

	public void setClub(String club) {
		Club = club;
	}

	public String getLicence() {
		return Licence;
	}

	public void setLicence(String licence) {
		Licence = licence;
	}

	@Override
	public String toString() {
		return "Joueur [Nom=" + Nom + ", Prenom=" + Prenom + ", Poste=" + Poste + ", Numero=" + Numero
				+ ", Dtenaissance=" + Dtenaissance + ", Club=" + Club + ", Licence=" + Licence + "]";
	}
	
}

