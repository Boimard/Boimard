package eu.hautil.JoueursDAO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import eu.hautil.Joueur.DAOException;
import eu.hautil.Joueur.Joueur;
import java.sql.ResultSet;
import java.util.ArrayList;
public class JoueursDAO {
	
	private Connection getConnection() throws ClassNotFoundException, SQLException {
		Connection conn = null;
		String driver = "com.mysql.cj.jdbc.Driver";
		String url = "jdbc:mysql://sio-hautil.eu:3306/boimak?user=boimak&password=20000315&serverTimezone=UTC";
		String login = "boimak";
		String password = "20000315";
		Class.forName(driver);
		System.out.println("driver ok");
		conn = DriverManager.getConnection(url,login,password);
		System.out.println("Connexion ok");
		return conn;
	}
	public void ajouterJ()throws DAOException{
		PreparedStatement pstmt = null;
		try {
		pstmt = getConnection().prepareStatement("INSERT INTO JoueurFoot VALUES (?,?,?,?,?,?,?)");
		pstmt.setString(1,"Pogba");
		pstmt.setString(2,"Paul");
		pstmt.setString(3,"Milieu de terrain");
		pstmt.setInt(4,15);
		pstmt.setDate(5,java.sql.Date.valueOf("1982-11-23"));
		pstmt.setString(6,"Manchester United");
		pstmt.setString(7,"19973356");
		int res=pstmt.executeUpdate();
		System.out.println(res);
		}
		catch(SQLException e) {
			DAOException ex = new DAOException ("Erreur SQL",e);
			throw ex;
		}
		catch(ClassNotFoundException e) {
			DAOException ex = new DAOException("Erreur Driver",e);
			throw ex;
		}
		finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public void supprJ() throws DAOException{
		try {
		int compteur = 0;
		PreparedStatement pstmt2 = getConnection().prepareStatement("DELETE FROM JoueurFoot WHERE Licence = 14529278");
		pstmt2.executeUpdate();
		System.out.println(pstmt2);
		compteur = compteur + 1;
		System.out.println("Lignes supprimées : " + compteur);
		}
		catch(SQLException e){
			DAOException ex = new DAOException ("Erreur SQL",e);
			throw ex;
		}
		catch(ClassNotFoundException e) {
			DAOException ex = new DAOException ("Erreur Driver",e);
			throw ex;
		}
	}
	public ArrayList <Joueur> rechercheAllJ() throws DAOException{
		ArrayList <Joueur> list = new ArrayList <Joueur>();
		try {
			PreparedStatement pstmt3 = getConnection().prepareStatement("SELECT * FROM JoueurFoot");
			ResultSet result = pstmt3.executeQuery();
			while (result.next()) {
			Joueur j = new Joueur(result.getString("Nom"),result.getString("Prenom"),result.getString("Poste"),result.getInt("Numero"),result.getDate("Date Naissance"),result.getString("Club"),result.getString("Licence"));
			list.add(j);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			DAOException ex = new DAOException (" Erreur SQL",e);
			e.printStackTrace();
			throw ex;
		}catch (ClassNotFoundException e) {
			DAOException ex =new DAOException ("Erreur Driver",e);
			throw ex;
		}
		return list;
	}
	private PreparedStatement prepareStatement(String reqjou) {
		return null;
	}
}
