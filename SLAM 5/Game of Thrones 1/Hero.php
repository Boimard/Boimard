<?php
	class Hero extends characters{
		private $nomActeur;
		
		function __construct($id,$nom,$dateNaiss,$dateMort,$culture,$nomActeur){
			parent::__construct($id,$nom,$dateNaiss,$dateMort,$culture);
			$this->nomActeur=$nomActeur;
		}
		public function getNomActeur(){
			return $this->nomActeur;
		}
		public function setNomActeur($nomActeur){
			$this->nomActeur=$nomActeur;
		}
		public function __toString(){
			echo parent:: __toString();
			return " Nom de l'acteur = ".$this->nomActeur;
		}
	}
?>