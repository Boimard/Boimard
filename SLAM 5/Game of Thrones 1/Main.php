<?php
	require_once("Region.php");
	require_once("Noble.php");
	require_once("Maison.php");
	require_once("characters.php");
	require_once("Culture.php");
	require_once("Hero.php");
	$region1 = new Region(1,"Terre de la couronne","Westeros");
	$region2 = new Region(2,"Le nord","Westeros");
	$culture1 = new Culture(1,"Andals");
	$culture2 = new Culture(2,"Nordien");
	$culture3 = new Culture(3,"Sothoryos");
	$culture4 = new Culture(4,"Dothraki");
	$maison1 = new Maison("Lannister", "Un Lannister paie toujours ses dettes !","Un lion","10000 ans",1,$region1);
	$maison2 = new Maison("Stark","L'hiver vient","Un loup","8000 ans",2,$region2);
	$personnage1 = new Noble(1,"Cersei Lannister","261-07-15","En vie",$culture1,$maison1,"Robert Baratheon","Tywin Lannister","Joanna Lannister");
	$GOT = array(1 =>$personnage1,
				 2 => new Hero(2,"Ygrid","000-00-00","301-02-27",$culture2,"Rose Leslie"),
				 3 => new Noble(3,"Arya Stark","287-04-08","En vie",$culture2,$maison2,"Non mariée","Ned Stark","Catelyn Stark"),
				 4 => new Hero(4,"Missandei","289-03-18","En vie",$culture3,"Natalie Emmanuel"),
				 5 => new Noble(5,"Tyrion Lannister","0000-00-00","En vie",$culture1,$maison1,"Non mariée","Tywin Lannister","Joanna Lannister"),
				 6 => new Hero(6,"Khal Drogo","0000-00-00","0000-00-00",$culture4,"Jason Momoa"));
?>