<?php
	require_once("Region.php");
	class Maison{
		private $nom;
		private $devise;
		private $armoirie;
		private $dateFond;
		private $id;
		private $region;
		
		function __construct($nom, $devise, $armoirie, $dateFond, $id, $region){
			$this->nom=$nom;
			$this->devise=$devise;
			$this->armoirie=$armoirie;
			$this->dateFond=$dateFond;
			$this->id=$id;
			$this->region=$region;
		}
		public function getNom(){
			return $this->nom;
		}
		public function setNom(){
			$this->nom=$nom;
		}
		public function getDevise(){
			return $this->devise;
		}
		public function setDevise(){
			$this->devise=$devise;
		}
		public function getArmoirie(){
			return $this->armoirie;
		}
		public function setArmoirie(){
			$this->armoirie=$armoirie;
		}
		public function getDateFond(){
			return $this->dateFond;
		}
		public function setDateFond(){
			$this->dateFond=$dateFond;
		}
		public function getId(){
			return $this->id;
		}
		public function setId(){
			$this->id=$id;
		}
		public function getRegion(){
			return $this->region;
		}
		public function setRegion(){
			$this->region=$region;
		}
		public function __toString(){
			return  "Nom de la maison = ".$this->nom." Devise de la maison = ".$this->devise." Armoirie = ".$this->armoirie." Date de fondation = ".$this->dateFond." Id de la maison = ".$this->id." Région = ".$this->region;
		}
	}
?>