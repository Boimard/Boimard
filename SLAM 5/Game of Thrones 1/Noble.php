<?php
	require_once("characters.php");
	class Noble extends characters{
		private $maison;
		private $conjoint;
		private $pere;
		private $mere;
		
		function __construct($id,$nom,$dateNaiss,$dateMort,$culture,$maison,$conjoint,$pere,$mere){
			parent::__construct($id,$nom,$dateNaiss,$dateMort,$culture);
			$this->maison=$maison;
			$this->conjoint=$conjoint;
			$this->pere=$pere;
			$this->mere=$mere;
		}
		public function getMaison(){
			return $this->maison;
		}
		public function setMaison(){
			$this->maison=$maison;
		}
		public function getConjoint(){
			return $this->conjoint;
		}
		public function setConjoint(){
			$this->conjoint=$conjoint;
		}
		public function getPere(){
			return $this->pere;
		}
		public function setPere(){
			$this->pere=$pere;
		}
		public function getMere(){
			return $this->mere;
		}
		public function setMere(){
			$this->mere=$mere;
		}
		public function __toString(){
			echo parent:: __toString();
			return " Maison = ".$this->maison." Conjoint(e) = ".$this->conjoint." Pere = ".$this->pere." Mere = ".$this->mere;
		}
	}
?>