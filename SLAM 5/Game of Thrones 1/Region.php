<?php
	class Region {
		private $id;
		private $libelle;
		private $pays;
		
		function __construct($id, $libelle, $pays){
			$this->id=$id;
			$this->libelle=$libelle;
			$this->pays=$pays;
		}
		public function getId(){
			return $this->id;
		}
		public function setId(){
			$this->id=$id;
		}
		public function getLibelle(){
			return $this->libelle;
		}
		public function setLibelle(){
			$this->libelle=$libelle;
		}
		public function getPays(){
			return $this->pays;
		}
		public function setPays(){
			$this->pays=$pays;
		}
		public function __toString(){
			return "Id de la région = ".$this->id." Nom de la région = ".$this->libelle." Pays = ".$this->pays;
		}
	}