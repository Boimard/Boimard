<?php
	require_once("Culture.php");
	abstract class characters{
		private $id;
		private $nom;
		private $dateNaiss;
		private $dateMort;
		private $culture;
		public static $compteur = 0;
		
		function __construct($id, $nom, $dateNaiss, $dateMort, $culture){
			$this->id=$id;
			$this->nom=$nom;
			$this->dateNaiss=$dateNaiss;
			$this->dateMort=$dateMort;
			$this->culture=$culture;
			self::$compteur++;
		}
		public function getId(){
			return $this->id;
		}
		public function setId(){
			$this->id=$id;
		}
		public function getNom(){
			return $this->nom;
		}
		public function setNom(){
			$this->nom=$nom;
		}
		public function getDateNaiss(){
			return $this->dateNaiss;
		}
		public function setDateNaiss(){
			$this->dateNaiss=$dateNaiss;
		}
		public function getDateMort(){
			return $this->dateMort;
		}
		public function setDateMort(){
			$this->dateMort=$dateMort;
		}
		public function getCulture(){
			return $this->culture;
		}
		public function setCulture(){
			$this->culture=$culture;
		}
		public function __toString(){
			return " Identifiant = ".$this->id." Nom = ".$this->nom." Date de Naissance = ".$this->dateNaiss." Date de Mort = ".$this->dateMort. " Culture = ".$this->culture;
		}
	}
?>
