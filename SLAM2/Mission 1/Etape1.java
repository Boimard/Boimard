public class Voyage {
	int age;
	String nom;
	Voyage (int vage, String vnom){
		age=vage;
		nom=vnom;
	}
	Voyage (){
		age = 0;
		nom = "Miguel";
	}
	void afficher (){
		System.out.println("nom :" + nom +" " + "Age : " + age);
	}
}

----------------------------------------------------------------------------------------------------------------------------------------

import java.util.Scanner;
public class Voyageur {
	public static void main (String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez votre nom :");
		String nom = sc.nextLine();
		System.out.println("Entrez votre age :");
		int age = sc.nextInt();
		Voyage a = new Voyage (age, nom);
		Voyage b = new Voyage (40, "Miguel");
		a.afficher();
		b.afficher();
	}
}
