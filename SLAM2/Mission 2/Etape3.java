public class Bagage {
    private int Numero;
    private String Couleur;
    private int Poids;

    Bagage(int Numero, String Couleur, int Poids){
        this.setNumero(Numero);
        this.setCouleur(Couleur);
        this.setPoids(Poids);
    }

    public void setNumero (int Numero){ this.Numero = Numero; }

    public void setCouleur(String Couleur) {this.Couleur = Couleur; }

    public void setPoids (int Poids) {this.Poids = Poids; }

    public int getNumero() {return Numero; }

    public String getCouleur() {return Couleur; }

    public int getPoids() {return Poids; }

    void afficher() {
        System.out.println("Votre bagage porte le numéro " + Numero + " est " + Couleur + " et pèse " + Poids +"kg");
    }
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
import java.util.Scanner;
public class Truc2 {
    public static void main ( String [] args) {
        Scanner sc = new Scanner(System.in);
        int i = 1;
        System.out.println("Avez vous des bagages ?");
        String rep = sc.nextLine();
        if (rep.equals("non")) {
            System.exit(0);
        }
        System.out.println("Veuillez saisir votre numéro de bagage");
        int Numero = sc.nextInt();
        Bagage a = null;
        System.out.println("Veuillez indiquer la couleur de votre bagage");
        String Couleur = sc.next();
        System.out.println("Veuillez indiquer le poids de votre bagage");
        int Poids = sc.nextInt();
        a = new Bagage(Numero, Couleur, Poids);
        a.setNumero(Numero);
        a.setCouleur(Couleur);
        a.setPoids(Poids);
        a.afficher();
    }
    }