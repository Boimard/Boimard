import java.util.ArrayList;
import java.util.Scanner;
public class Mission3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Voyageur> listevoyageur = new ArrayList<Voyageur>(); //Création de la liste. Type de la liste = Voyage car on désire une liste de voyageur
        int age;
        String nom;
        System.out.println("Etes vous un nouveau voyageur ? Oui/Non");
        String reponse = sc.next();

        while (reponse.equals("oui")) { //boucle test de l'initialisation du voyageur
            System.out.println("Saisissez votre nom");
            nom = sc.next();
            System.out.println("Saisissez votre age");
            age = sc.nextInt();
            Voyageur v = new Voyageur(age, nom); //On initialise un nouveau voyageur
            listevoyageur.add(v);
            System.out.println("Autre voyageur ? Oui/Non");
            reponse = sc.next();
        }
        for (int i = 0; i < listevoyageur.size(); i++) { //Affichage de la liste
            Voyageur v = listevoyageur.get(i);
            v.afficher();
        }
        System.out.println("Veuillez saisir le nom a rechercher");
        nom = sc.next();
        for (Voyageur v : listevoyageur) {
            if (nom.equals(v.getnom())) {
                v.afficher();
            }
        }
        System.out.println("Veuillez saisir le nom à supprimer");
        nom = sc.next();
            for (Voyageur v : listevoyageur) {
                if (nom.equals(v.getnom())) {
                    listevoyageur.remove(v);
                    for (int j = 0; j < listevoyageur.size(); j++) { //Affichage de la liste
                        v = listevoyageur.get(j);
                        v.afficher();
                    }
                }
            }
        }
    }
    
    
    public class Voyageur {
    private int age;
    private String nom;
    private String categorie;
    private AdressePostale adresse;
    private Bagage bagage;

    /**
     * Modifie l'adresse du voyageur
     * @param adresse Adresse du voyageur
     */
    public void setadresse(AdressePostale adresse){
        this.adresse = adresse;
    }

    /**
     * Récupère l'adresse du voyageur
     * @return Adresse du voyageur
     */
    public AdressePostale getadresse() {
        return adresse;
    }

    /**
     * Modifie le bagage
     * @param bagage Bagage du voyageur
     */
    public void setBagage (Bagage bagage) {this.bagage = bagage; }

    /**
     * Récupère le bagage
     * @return Bagage du voyageur
     */
    public Bagage getbagage() {return bagage; }

    /**
     * Modifie le nom du voyageur
     * @param nom Nom du voyageur
     */
    public void setNom(String nom) {
        if (nom.length() >= 2) {
            this.nom = nom;

        }
    }

    /**
     * Determination de la categorie en fonction de l'age
     * @param age Age du voyageur
     */
    public void setAge  (int age) {
        if (age > 0){
            this.age=age;
            if (age<= 2){
                categorie ="nourisson";
            }
            else if (age >= 3 && age <= 12){
                categorie="Enfant";
            }
            else if (age >= 12 && age<= 55){
                categorie="Adulte";
            }
            else if (age> 55){
                categorie="Senior";
            }
        }
    }
    Voyageur(int age, String nom) {
        this.setNom(nom);
        this.setAge(age);
    }
    Voyageur(){
        age = 40;
        nom = "Miguel";
        categorie = "Adulte";
        if (age<= 2){
            categorie ="nourisson";
        }
        else if (age<= 12){
            categorie="Enfant";
        }
        else if (age<= 55){
            categorie="Adulte";
        }
        else if (age> 55){
            categorie="Senior";
        }
    }

    /**
     * Affiche les résultats du voyageur
     */
    void afficher () {
        System.out.println("nom : " + nom + " Age : " + age + " Categorie : " + categorie);
        if (adresse != null) {
            this.adresse.afficher();
        }
        if (bagage != null) {
            this.bagage.afficher();
        }
    }

    /**
     * Récupère le nom du voyageur
     * @return le nom du voyageur
     */
    public String getnom() {return nom; }
    }
