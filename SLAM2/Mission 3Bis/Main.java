import eu.hautil.mission3.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int verifrech = 0;
        int verifrech2 = 0;
        AdressePostale a = new AdressePostale("21 rue du soleil", "Paris", "75000");
        a.afficher();
        System.out.println(a.getAdresse());
        System.out.println(a.getVille());
        System.out.println(a.getCP());
        a.setAdresse("12 boulevard du ciel");
        a.afficher();

        //ETAPE 1
        // Créer une adresse postale (no rue xxx, ville, CP)
        // afficher l'adresse postale à l'aide de la méthode "afficher"
        // afficher les informations de l'adresse postales à l'aide des getters
        // modifier une propriété de l'adresse postale
        // réafficher l'adresse pour constater le changement

        Bagage b = new Bagage(45, "vert", 5);
        b.afficher();
        System.out.println(b.getNumero());
        System.out.println(b.getCouleur());
        System.out.println(b.getPoids());
        b.setCouleur("rouge");
        b.afficher();

        //ETAPE 2
        // créer un bagage (numéro, couleur et poids)
        // afficher le bagage à l'aide de la méthode "afficher"
        // afficher les informations du bagage à l'aide des getters
        // modifier une propriété du bagage
        // réafficher le bagage pour constater le changement

        Voyageur v = new Voyageur("Miguel", 49);
        v.afficher();
        v.setAdresse(a);
        v.afficher();
        System.out.println(v.getNom());
        System.out.println(v.getAge());
        System.out.println(v.getAdresse().getAdresse());
        System.out.println(v.getAdresse().getVille());
        System.out.println(v.getAdresse().getCP());
        v.setNom("Pedro");
        v.getAdresse().setVille("Tours");
        v.getAdresse().setCP("65286");
        v.afficher();

        //ETAPE 3
        // créer un voyageur (nom, age)
        // afficher le voyageur à l'aide de la méthode "afficher" (on constate qu'il manque une adresse !)
        // ajouter une adresse à ce voyageur (celle créée à l'étape 1)
        // réafficher le voyageur à l'aide la méthode "afficher"
        // afficher les propriétés du voyageur à l'aide des getters
        // modifier le nom de ce voyageur
        // modifier la ville de ce voyageur
        // réafficher le voyageur à l'aide la méthode "afficher" pour constater le changement

        v.setBagage(b);
        v.afficher();
        System.out.println(v.getNom());
        System.out.println(v.getAge());
        System.out.println(v.getAdresse().getAdresse());
        System.out.println(v.getAdresse().getVille());
        System.out.println(v.getAdresse().getCP());
        System.out.println(v.getBagage().getNumero());
        System.out.println(v.getBagage().getCouleur());
        System.out.println(v.getBagage().getPoids());
        v.setAge(8);
        v.getBagage().setCouleur("Rose");
        v.afficher();

        //ETAPE 4
        // Compléter le voyageur de l'étape 3
        // ajouter un bagage à ce voyageur (celui créé à l'étape2)
        // afficher le voyageur à l'aide la méthode "afficher"
        // afficher les propriétés du voyageur à l'aide des getters
        // modifier l'âge du voyageur
        // modifier la couleur du bagage du voyageur
        // réafficher le voyageur à l'aide la méthode "afficher" pour constater le changement

        ArrayList<Voyageur> voyageurs = new ArrayList<>();
        Voyageur v1 = new Voyageur("Jean",69);
        AdressePostale a1 = new AdressePostale();
        v1.setAdresse(a1);
        voyageurs.add(v1);
        Voyageur v2 = new Voyageur("Jerome", 13);
        AdressePostale a2 = new AdressePostale();
        v2.setAdresse(a2);
        voyageurs.add(v2);
        Voyageur v3 = new Voyageur("Jack", 42);
        AdressePostale a3 = new AdressePostale();
        v3.setAdresse(a3);
        voyageurs.add(v3);
        Voyageur v4 = new Voyageur("Louis", 6);
        AdressePostale a4 = new AdressePostale();
        v4.setAdresse(a4);
        voyageurs.add(v4);
        Voyageur v5 = new Voyageur("Loic",2);
        AdressePostale a5 = new AdressePostale();
        v5.setAdresse(a5);
        voyageurs.add(v5);
        for (int i=0; i<voyageurs.size(); i++){
            Voyageur voyageur = voyageurs.get(i);
            voyageur.afficher();
        }

        //ETAPE 5
        // Créer une liste dynamique de 5 voyageurs (nom, age, adresse)
        // afficher tous les voyageurs de la liste à l'aide de la méthode "afficher"

        System.out.println("Veuillez saisir un voyageur");
        String nomvoy = sc.nextLine();
        for (int i = 0; i < voyageurs.size(); i++){
            if (nomvoy.equals(voyageurs.get(i).getNom())){
                voyageurs.get(i).afficher();
                verifrech = 1; // Flag pour diviser les actions du for et du if
            }
        }
        if (verifrech == 0){
            System.out.println("Erreur ! La personne renseignée n'existe pas"); // Message d'erreur a l'aide du flag
        }

        //ETAPE 6 (Recherche)
        // Demander à l'utilisateur de saisir un nom de voyageur
        // Chercher ce voyageur dans la liste de l'étape5
        // afficher le voyageur trouvé ou un message de regret

        System.out.println("Veuillez saisir de nouveau un voyageur");
        String nomvoy2 = sc.nextLine();
        for ( int j = 0; j < voyageurs.size(); j++) {
            if (nomvoy2.equals(voyageurs.get(j).getNom())) {
                voyageurs.get(j).afficher();
                verifrech2 = 1;
                System.out.println("Voulez vous réellement supprimer ce voyageur ? Entrer 1 pour oui et 2 pour non");
                int testsupp = sc.nextInt();
                while (testsupp == 1 || testsupp == 2) {
                    if (testsupp == 1) {
                        voyageurs.remove(j); 
                        System.out.println("Le voyageur a été correctement supprimé");
                        for (int i = 0; i < voyageurs.size(); i++) {
                            Voyageur voyageur = voyageurs.get(i);
                            voyageur.afficher();
                            testsupp = 0;
                        }
                    } else if (testsupp == 2) {
                        System.out.println("Le voyageur n'a pas été supprimé");
                        for (int i = 0; i < voyageurs.size(); i++) {
                            Voyageur voyageur = voyageurs.get(i);
                            voyageur.afficher();
                            testsupp = 0;
                        }
                    }

                }
            }
        }
        if (verifrech2 == 0){
            System.out.println("Erreur ! La personne renseignée n'existe pas");
        }

        //ETAPE 7 (recherche + suppression)
        // Demander à l'utilisateur de saisir un nom de voyageur
        // Chercher ce voyageur dans la liste de l'étape5
        // supprimer le voyageur trouvé et afficher un message de confirmation  ou afficher un message de regret
        // réafficher la liste des voyageurs

    }
}