import java.sql.Connection;
import java.sql.DriverManager;
import  java.sql.Statement;
import  java.sql.ResultSet;
public class JBDCSimple {
    public static void main (String[] args){
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            System.out.println("Driver ok");
            Connection connection = DriverManager.getConnection("jdbc:mariadb://sio-hautil.eu:3306/boimak?user=boimak&password=20000315");
            System.out.println("Connection ok");
            Statement stmt = connection.createStatement();
            String req1 = "INSERT INTO bts_professeurs VALUES (32, 'Abdelmoula', 'SLAM')"; //Ecriture de la requête SQL
            int res = stmt.executeUpdate(req1); //Execution de la requête et mise a jour de la table
            System.out.println("nb de modifs =" + res); //Affichage du nb de modifs
        }
        catch (Exception e){
            e.printStackTrace();
        }


    }
}
