import java.sql.Connection;
import java.sql.DriverManager;
import  java.sql.Statement;
import  java.sql.ResultSet;
public class JBDCSimple {
    public static void main (String[] args){
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            System.out.println("Driver ok");
            Connection connection = DriverManager.getConnection("jdbc:mariadb://sio-hautil.eu:3306/boimak?user=boimak&password=20000315");
            System.out.println("Connection ok");
            Statement stmt = connection.createStatement();
            String req2 = "SELECT nom, specialite FROM bts_professeurs";
            ResultSet result = stmt.executeQuery(req2);
            while (result.next()){ //result.next permet de naviguer dans le tableau|C'est un booléen quand une colonne est pleine il l'affiche vide il arrete la boucle 
                System.out.println("Nom : " + result.getString(1));
                System.out.println("Spécialité : " + result.getString(2));
            }
        }
        catch (Exception e){
            e.printStackTrace(); //permet d'afficher l'exception
        }


    }
}
